<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 04.03.2018
 * Time: 16:08
 */

include"../models/Product.php";
/*

// Tell PHP that we're using UTF-8 strings until the end of the script
mb_internal_encoding('UTF-8');

// Tell PHP that we'll be outputting UTF-8 to the browser
mb_http_output('UTF-8');


*/

class ProductController
{
    private function  getDB(){

        $db = new PDO('mysql:host=localhost:3306;dbname=juniortest;charset=utf8mb4', 'root', '');

        return $db;
    }
    private function createProduct($data){
        switch($data["type"]){
            case "CD":
                $product = new CD();
                $product = $product->setValues($data,$product);
                break;
            case "Book":
                $product = new Book();
                $product = $product->setValues($data,$product);
                break;
            case "Furniture":
                $product = new Furniture();
                $product = $product->setValues($data,$product);
                break;
            default:
                $product = new Product();
                $product = $product->setValues($data,$product);
                break;
        }
     return $product;
    }

    public function getProducts($count, $offset){

            $db = $this->getDB();

            $statement = $db->query(
                "SELECT sku,name,price,type
                          FROM products
                          ORDER BY sku
                          LIMIT ".$offset.",".$count
            );
            for ($i = 0; $i < $count; $i++) {
                if (!$row = $statement->fetch(PDO::FETCH_ASSOC))
                    break;
                echo '<pre>';
                var_dump($row);
                echo '</pre>';
            }
    }


}

$controller = new ProductController();

$data = [
    "name" => "Hitchikers Guide to Galaxy",
    "price" => 12.99,
    "special" => 1.5,
    "type" => ""
];

$controller->getProducts(3, 0);
$controller->getProducts(3, 3);