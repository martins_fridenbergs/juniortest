<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 04.03.2018
 * Time: 14:53
 */

class Product
{

    /**
     * @type integer
     */
    private $sku;

    /**
     * @type string
     */
    private $name;

    /**
     * @type float
     */
    private $price;

    /**
     * @type integer
     */
    private $type;

    /**
     * @return int
     */
    public function getSku(): int
    {
        return $this->sku;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @param array $data
     * @param mixed $product
     * @return mixed
     */
    public function setValues($data,$product){

        $product->setName($data["name"]);
        $product->setPrice($data["price"]);

        return $product;
    }


}

class CD extends Product{

    /**
     * Size in MB
     * @type mixed
     */
    private $size;

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size): void
    {
        $this->size = $size;
    }

    /**
     * @param array $data
     * @param CD $product
     * @return CD
     */
    public function setValues($data,$product){

        $product = parent::setValues($data,$product);

        $product->setType($data["type"]);
        $product->setSize($data["special"]);

        return $product;
    }

}

class Book extends Product{

    /**
     * Weight in kg
     * @type mixed
     */
    private $weight;

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @param array $data
     * @param Book $product
     * @return Book
     */
    public function setValues($data,$product){

        $product = parent::setValues($data,$product);

        $product->setType($data["type"]);
        $product->setWeight($data["special"]);

        return $product;
    }

}

class Furniture extends Product{

    /**
     * Dimensions (HxWxL)
     * @type mixed
     */
    private $dimensions;

    /**
     * @return mixed
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * @param mixed $dimensions
     */
    public function setDimensions($dimensions): void
    {
        $this->dimensions = $dimensions;
    }

    public function setValues($data,$product){

        $product = parent::setValues($data,$product);

        $product->setType($data["type"]);
        $product->setDimensions($data["special"]);

        return $product;
    }

}